package com.kravtsov.time_tracker4;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.ActionBarActivity;
import android.text.format.Time;
import android.util.Log;
import android.util.SparseArray;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 26.05.2015.
 */
public class TimeChecker  extends ActionBarActivity {

    //-----global var-----
    DBHelper dbHelper;
    public Context MainContext;
    public  MainActivity MyMain;
    //public StructSendObj  structSendObj=new StructSendObj();
    //Массив категорий

    //

    class Struct_list_category
    {
        public int Categoty;
        public int time_dely;
        public int time_ralai;
    }

    public TimeChecker(Context mainContext,MainActivity myMain)
    {
        MainContext = mainContext;
        MyMain=myMain;
        dbHelper = new DBHelper(MainContext);
    }


    //




    public void ResetDb() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        Log.d("LOG_TAG", "--- Table table_action ---");
        try {
            c = db.rawQuery("drop table table_action;", null);
            c = db.rawQuery("drop table table_category;", null);
            c = db.rawQuery("drop table table_date;", null);
        }
        catch (Exception ex)
        {
            Log.d("LOG_TAG", "ResetDb Error "+ex.toString());
        }
        c.close();
        dbHelper.close();
    }

    //
    public void SetCategory()
    {
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String[] choose=MainContext.getResources().getStringArray(R.array.Tipelist);
        for (String i:choose)
        {
            cv.clear();
            cv.put("category", i);
            db.insert("table_category", null, cv);
        }
        // Описание курсора
        Cursor c;
        Log.d("LOG_TAG", "--- Table table_category ---");
        c = db.query("table_category", null, null, null, null, null, null);
        logCursor(c);
        c.close();
        Log.d("LOG_TAG", "--- ---");

        dbHelper.close();
    }

    public void Incert_Action(long CalStart,long CalStop,int TypeOfCommand)
    {
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String[] choose=MainContext.getResources().getStringArray(R.array.Tipelist);

        //Date DateForRemember.get;
        int test_int= (int) CalStart;
        Calendar caltest=Calendar.getInstance();
        //caltest.set(Calendar.MILLISECOND,(int)( CalStart*1000));
        caltest.setTimeInMillis(CalStart*1000);
        Log.d("LOG_TAG", "--- ---");
        Log.d("LOG_TAG", "Added time start");
        Log.d("LOG_TAG", "Start - "+caltest.toString());
        Log.d("LOG_TAG", "--- ---");
        cv.put("id_category", TypeOfCommand);
       // cv.put("time_start", TimeForRemember);
        cv.put("date_start", CalStart);
        cv.put("date_stop",CalStop);
        db.insert("table_action", null, cv);


        Cursor c;
        Log.d("LOG_TAG", "--- Table table_action ---");
        c = db.query("table_action", null, null, null, null, null, null);
        logCursor(c);
        c.close();
        Log.d("LOG_TAG", "--- ---");

        dbHelper.close();
    }

    public int IncertWithMulti(long CalStart,long CalStop,int TypeOfCommand)
    {
        int result=0;
        ArrayList<SparseArray> TimeCut=Calculate.cut_for_day(CalStart, CalStop);

        for (SparseArray SparseArrayElement:TimeCut)
        {
            int k=IncertWithSort(Integer.parseInt(SparseArrayElement.get(0).toString()),Integer.parseInt(SparseArrayElement.get(1).toString()),TypeOfCommand);
        }
        return result;
    }

    public int IncertWithSort(long CalStart,long CalStop,int TypeOfCommand)
    {
        Log.d("LOG_TAG", "--- IncertWithSort ---");
        int result=0;
        Cursor c=null;
        int modify1=0;
        int modify2=0;
        int modify3=0;
        int modify4=0;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            /*
            c = db.query("table_action", null,
                    "date_start between " + CalStart + " and " +  CalStop + " OR "+
                    "date_stop between "+ CalStart + " and " +  CalStop + " OR "+
                    CalStart+" between date_start and date_stop "+" OR "+
                    CalStop+" between date_start and date_stop "
                    , null, null, null, null);                // Работает
                    */


            c = db.query("table_action", null,
                    "date_start >= " + CalStart + " and date_stop <= " +  CalStop
                    , null, null, null, null);                // Работает
            modify3=work_cursor_list_interval(db,c,CalStart,CalStop,TypeOfCommand,3);                           //тип 3 поглощение

            c = db.query("table_action", null,
                    "date_start between " + CalStart + " and " +  CalStop+" AND "+
                            "date_start < "+CalStop
                    , null, null, null, null);                // Работает
            modify2=work_cursor_list_interval(db,c,CalStart,CalStop,TypeOfCommand,2);                           //тип 2 обрезка слева

            c = db.query("table_action", null,
                    "date_stop between "+ CalStart + " and " +  CalStop+" AND "+
                            "date_stop > "+CalStart
                    , null, null, null, null);                // Работает
            modify1=work_cursor_list_interval(db,c,CalStart,CalStop,TypeOfCommand,1);                           //тип 1 обрезка справа

            c = db.query("table_action", null,
                    CalStart+" between date_start and date_stop "+" AND "+
                            CalStop+" between date_start and date_stop "
                    , null, null, null, null);                // Работает
            modify4=work_cursor_list_interval(db,c,CalStart,CalStop,TypeOfCommand,4);                           //тип 4 разбиение



            if((modify2!=2)&&(modify1!=1)&&(modify4!=4))
            {
                Log.d("LOG_TAG", "IncertWithSort обычное добавление ");
                Incert_Action(CalStart,CalStop,TypeOfCommand);
            }
        }
        catch (Exception ex)
        {
            Log.d("LOG_TAG", "IncertWithSort query Err "+ex.toString());
        }

        c.close();
        Log.d("LOG_TAG", "--- ---");

        dbHelper.close();
        return result;
    }

    public ArrayList<StructSendObj> Request2_Date(long CalDay)
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String coll[]={"id","date_start"};
        String lineC="";
        Calendar caltest=Calendar.getInstance();
        String[]  TextForRec;
        ArrayList<StructSendObj> Actions_on_time;
        Cursor c=null;
        Log.d("LOG_TAG", "--- Request2_Date ---");
        //int Hi = db.delete("table_action", null, null);           //Очистка бд
        try {
            c = db.query("table_action", null, "date_start between " + Calculate.StartStop_day(CalDay, true) + " and " +
                    Calculate.StartStop_day(CalDay, false), null, null, null, null);                // Работает
        }
        catch (Exception ex)
        {
            Log.d("LOG_TAG", "Request2_Date query Err "+ex.toString());
        }
        Actions_on_time=work_cursor_list_all(c);
        c.close();
        Log.d("LOG_TAG", "--- ---");

        dbHelper.close();

        return  Actions_on_time;
    }

    //c = db.query("table_action", null, StartStop_day(CalDay,true)+" < date_start < "+  StartStop_day(CalDay,false), null, null, null, null); // Не работает
    //c = db.query("table_action", null, CalDay+" < date_start", null, null, null, null);                               //Как то работает
    //c = db.rawQuery("SELECT table_date datetime(1092941466, 'unixepoch', 'localtime');", null);
    //c = db.rawQuery("SELECT date_start from table_action where id_category = 1;", null);
    //c = db.rawQuery("SELECT date_start from table_action where date_start between date('now') and date('2014-11-11');", null);
    //c = db.rawQuery("SELECT date_start from table_action where date_start > 2404421262;", null);
    //SELECT date('now','start of year','+9 months','weekday 2');
    //logCursor(c);


                                                                                                    //работает с курсором
    private int work_cursor_list_interval(SQLiteDatabase db,Cursor c,long CalStart,long CalStop,int TypeOfCommand,int type)
    {
        String lineC="";

        int cn_category=0;
        int cn_date_start=0;
        int cn_date_stop=0;
        int cn_id=0;

        int result=0;


        Cursor curs=null;
        boolean cut=false;
        if (c != null) {
            if (c.moveToFirst()) {
                String str;
                do {
                    str = "";
                    for (String cn : c.getColumnNames()) {
                        str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
                        try
                        {
                            if (cn.contains("date_start")) {
                                cn_date_start=c.getInt(c.getColumnIndex(cn));
                            }
                            else if (cn.contains("id_category"))
                            {
                                cn_category =c.getInt(c.getColumnIndex(cn));

                            }
                            else if (cn.contains("date_stop")) {
                                cn_date_stop=c.getInt(c.getColumnIndex(cn));

                            }
                            else if(cn.contains("id")) {
                                cn_id=c.getInt(c.getColumnIndex(cn));

                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    Log.d("LOG_TAG", str);


                    switch (type) {
                        case 4:
                            result=time_updater_big(cn_id,cn_date_start,cn_date_stop,cn_category,CalStart,CalStop,TypeOfCommand,db,c);
                            break;
                        case 3:
                            result=time_updater_small(cn_id,cn_date_start,cn_date_stop,cn_category,CalStart,CalStop,TypeOfCommand,db,c);
                            break;
                        case 2:
                            result=time_updater_left(cn_id,cn_date_start,cn_date_stop,cn_category,CalStart,CalStop,TypeOfCommand,db,c);
                            break;
                        case 1:
                            result=time_updater_right(cn_id,cn_date_start,cn_date_stop,cn_category,CalStart,CalStop,TypeOfCommand,db,c);
                            break;
                    }



                    if (cut) {

                    }
                } while (c.moveToNext());
            }
        } else
            Log.d("LOG_TAG", "Cursor is null type "+type);
        return result;
    }


                                                                                                    //метод разбивает категорию
    private int time_updater_big(int id, int start, int stop, int cat,long CalStart,long CalStop,int TypeOfCommand, SQLiteDatabase db,Cursor c)
    {
        Log.d("LOG_TAG", "time_updater_big start "+id);
        int result=0;
        ContentValues cv =  new ContentValues();
        if (cat!=TypeOfCommand)
        {
            try {
                if(start!=CalStart) {                                                               //если начало не совпадает
                                                                                                    //меняем окончание
                    cv.put("date_stop", CalStart);
                    result = db.update("table_action", cv, "id=?", new String[]{id + ""});

                    if(stop!=CalStop) {                                                                 //если окончание не совпадает
                        if(stop>CalStop) {
                            cv = new ContentValues();
                            //вставляем окончание
                            cv.put("id_category", cat);
                            cv.put("date_start", CalStop);
                            cv.put("date_stop", stop);
                            db.insert("table_action", null, cv);
                        }
                    }

                }
                else
                {
                    cv.put("date_start", CalStop);
                    result = db.update("table_action", cv, "id=?", new String[]{id + ""});
                }
                /*
                cv = new ContentValues();
                                                                                                    //вставляем промежуток
                cv.put("id_category", TypeOfCommand);
                cv.put("date_start", CalStart);
                cv.put("date_stop",CalStop);
                db.insert("table_action", null, cv);
*/
/*
                if(stop!=CalStop) {                                                                 //если окончание не совпадает
                    cv = new ContentValues();
                                                                                                    //вставляем окончание
                    cv.put("id_category", cat);
                    cv.put("date_start", CalStop);
                    cv.put("date_stop", stop);
                    db.insert("table_action", null, cv);
                }
                */
            }
            catch (Exception ex)
            {
                Log.d("LOG_TAG", "work_cursor_list_interval_big Update "+ex.toString());
            }
        }
        else
        {
            result=4;
        }

                return result;
    }
                                                                                                    //метод поглащает категорию
    private int time_updater_small(int id, int start, int stop, int cat,long CalStart,long CalStop,int TypeOfCommand, SQLiteDatabase db,Cursor c)
    {
        int result=3;
        Log.d("LOG_TAG", "time_updater_small start");
        ContentValues cv =  new ContentValues();
            try {
                //удаляем
                cv.put("date_stop", CalStart);
                int k = db.delete("table_action", "id=?", new String[]{id + ""});

            }
            catch (Exception ex)
            {
                Log.d("LOG_TAG", "time_updater_small Update "+ex.toString());
            }

        return result;
    }

    private int time_updater_left(int id, int start, int stop, int cat,long CalStart,long CalStop,int TypeOfCommand, SQLiteDatabase db,Cursor c)
    {
        Log.d("LOG_TAG", "time_updater_left start");
        int result=0;
        ContentValues cv =  new ContentValues();
        if (cat!=TypeOfCommand)
        {
            try {
                //меняем начало
                cv.put("date_start", CalStop);
                int k = db.update("table_action", cv, "id=?", new String[]{id + ""});

                /*
                cv = new ContentValues();
                //вставляем промежуток
                cv.put("id_category", TypeOfCommand);
                cv.put("date_start", CalStart);
                cv.put("date_stop",CalStop);
                db.insert("table_action", null, cv);
                */

            }
            catch (Exception ex)
            {
                Log.d("LOG_TAG", "time_updater_left Update cat!=TypeOfCommand"+ex.toString());
            }
        }
        else
        {
            result=2;
            try {
                //меняем начало
                cv.put("date_start", CalStart);
                int k = db.update("table_action", cv, "id=?", new String[]{id + ""});

            }
            catch (Exception ex)
            {
                Log.d("LOG_TAG", "time_updater_left Update cat==TypeOfCommand"+ex.toString());
            }
        }

        return result;
    }

    private int time_updater_right(int id, int start, int stop, int cat,long CalStart,long CalStop,int TypeOfCommand, SQLiteDatabase db,Cursor c)
    {
        Log.d("LOG_TAG", "time_updater_right start");
        int result=0;
        ContentValues cv =  new ContentValues();
        if (cat!=TypeOfCommand)
        {
            try {
                //меняем начало
                cv.put("date_stop", CalStart);
                int k = db.update("table_action", cv, "id=?", new String[]{id + ""});


                /*
                cv = new ContentValues();
                //вставляем промежуток
                cv.put("id_category", TypeOfCommand);
                cv.put("date_start", CalStart);
                cv.put("date_stop",CalStop);
                db.insert("table_action", null, cv);
                */
            }
            catch (Exception ex)
            {
                Log.d("LOG_TAG", "time_updater_right Update cat!=TypeOfCommand"+ex.toString());
            }
        }
        else
        {
            result=1;
            try {
                //меняем начало
                cv.put("date_stop", CalStop);
                int k = db.update("table_action", cv, "id=?", new String[]{id + ""});

            }
            catch (Exception ex)
            {
                Log.d("LOG_TAG", "time_updater_right Update cat==TypeOfCommand"+ex.toString());
            }
        }

        return result;
    }

    private void work_cursor_list_interval(Cursor c)                                 //возвращает адаптер с полным списком
    {
        if (c != null) {
            if (c.moveToFirst()) {
                String str;
                do {
                    StructSendObj  structSendObj=new StructSendObj();
                    str = "";
                    for (String cn : c.getColumnNames()) {
                        str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
                        try {
/*
                            if (cn.contains("date_start")) {
                                //caltest.setTimeInMillis(Long.parseLong(c.getString(c.getColumnIndex(cn)))*1000);
                                lineC = "" + c.getString(c.getColumnIndex(cn));
                                structSendObj.Start_time=Integer.parseInt(c.getString(c.getColumnIndex(cn)));
                            } else if (cn.contains("date_stop")) {
                                lineC = lineC + "_/_" + c.getString(c.getColumnIndex(cn));
                                structSendObj.Stop_time=Integer.parseInt(c.getString(c.getColumnIndex(cn)));
                            }
                            else if (cn.contains("id_category")) {
                                lineC = lineC + "_/_" + c.getString(c.getColumnIndex(cn));
                                structSendObj.Categoty=Integer.parseInt(c.getString(c.getColumnIndex(cn)));
                            }
                            */
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    Log.d("LOG_TAG", str);
                } while (c.moveToNext());
            }
        } else
            Log.d("LOG_TAG", "Cursor is null");
    }

    private ArrayList<StructSendObj> work_cursor_list_all(Cursor c)                                 //возвращает адаптер с полным списком
    {
        ArrayList<StructSendObj> Actions_on_time = new ArrayList<StructSendObj>();
        String lineC="";
        if (c != null) {
            if (c.moveToFirst()) {
                String str;
                do {
                    StructSendObj  structSendObj=new StructSendObj();
                    str = "";
                    for (String cn : c.getColumnNames()) {
                        str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
                        try {

                            if (cn.contains("date_start")) {
                                //caltest.setTimeInMillis(Long.parseLong(c.getString(c.getColumnIndex(cn)))*1000);
                                lineC = "" + c.getString(c.getColumnIndex(cn));
                                structSendObj.Start_time=Integer.parseInt(c.getString(c.getColumnIndex(cn)));
                            } else if (cn.contains("date_stop")) {
                                lineC = lineC + "_/_" + c.getString(c.getColumnIndex(cn));
                                structSendObj.Stop_time=Integer.parseInt(c.getString(c.getColumnIndex(cn)));
                            }
                            else if (cn.contains("id_category")) {
                                lineC = lineC + "_/_" + c.getString(c.getColumnIndex(cn));
                                structSendObj.Categoty=Integer.parseInt(c.getString(c.getColumnIndex(cn)));
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    Actions_on_time.add(structSendObj);
                    lineC="";
                    Log.d("LOG_TAG", str);
                } while (c.moveToNext());
            }
        } else
            Log.d("LOG_TAG", "Cursor is null");


        return  Actions_on_time;
    }

    private ArrayList<StructSendObj> work_cursor_list_category(Cursor c)                                 //возвращает адаптер с полным списком
    {
        ArrayList<StructSendObj> Actions_on_time = new ArrayList<StructSendObj>();
        String lineC="";
        int cn_category=0;
        int cn_date_start=0;
        int cn_date_stop=0;
        if (c != null) {
            if (c.moveToFirst()) {
                String str;
                do {
                    StructSendObj  structSendObj=new StructSendObj();
                    str = "";
                    cn_category=0;
                    cn_date_start=0;
                    cn_date_stop=0;
                    for (String cn : c.getColumnNames()) {
                        str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
                        try {

                            if (cn.contains("date_start")) {
                                //caltest.setTimeInMillis(Long.parseLong(c.getString(c.getColumnIndex(cn)))*1000);
                                lineC = "" + c.getString(c.getColumnIndex(cn));
                                cn_date_start=Integer.parseInt(c.getString(c.getColumnIndex(cn)));
                            } else if (cn.contains("date_stop")) {
                                lineC = lineC + "_/_" + c.getString(c.getColumnIndex(cn));
                                cn_date_stop=Integer.parseInt(c.getString(c.getColumnIndex(cn)));
                            }
                            else if (cn.contains("id_category")) {
                                lineC = lineC + "_/_" + c.getString(c.getColumnIndex(cn));
                                cn_category=Integer.parseInt(c.getString(c.getColumnIndex(cn)));
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    Actions_on_time.add(structSendObj);
                    lineC="";
                    Log.d("LOG_TAG", str);
                } while (c.moveToNext());
            }
        } else
            Log.d("LOG_TAG", "Cursor is null");


        return  Actions_on_time;
    }

    public void ShowTime(long CalDay)
    {
        try
        {
            ContentValues cv = new ContentValues();
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String[] choose=MainContext.getResources().getStringArray(R.array.Tipelist);
            long myl=1L;
            Cursor c;
            Log.d("LOG_TAG", "--- Table date ---");
            c = db.query("table_date", null, null, null, null, null, null);
            //c = db.rawQuery("SELECT table_date datetime(1092941466, 'unixepoch', 'localtime');", null);
            //c = db.rawQuery("SELECT date from table_date where date between date('now') and date('2015-06-09');", null);
            c = db.rawQuery("SELECT date from table_date where date between date('now') and date('"+CalDay+"');", null);
            //SELECT date('now','start of year','+9 months','weekday 2');
            //logCursor(c);
            if (c != null) {
                if (c.moveToFirst()) {
                    String str;
                    do {
                        str = "";
                        for (String cn : c.getColumnNames()) {
                            str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");

                        }
                        Log.d("LOG_TAG", str);
                    } while (c.moveToNext());
                }
            } else
                Log.d("LOG_TAG", "Cursor is null");
            c.close();
            Log.d("LOG_TAG", "--- ---");

            dbHelper.close();
        }
        catch (Exception ex)
        {
            Log.d("LOG_TAG", "fail tread "+ex.toString());

        }
    }

    public void ShowAllDB()
    {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String[] choose=MainContext.getResources().getStringArray(R.array.Tipelist);

        try {

            Cursor c;
            c = db.query("table_action", null, null, null, null, null, null);
            logCursor(c);
            c.close();
            Log.d("LOG_TAG", "--- ---");

            c = db.query("table_category", null, null, null, null, null, null);
            logCursor(c);
            c.close();
            Log.d("LOG_TAG", "--- ---");

            c = db.query("table_date", null, null, null, null, null, null);
            logCursor(c);
            c.close();
            Log.d("LOG_TAG", "--- ---");
        }
        catch (Exception ex)
        {

        }
        dbHelper.close();
    }

    // вывод в лог данных из курсора
    void logCursor(Cursor c) {
        if (c != null) {
            if (c.moveToFirst()) {
                String str;
                do {
                    str = "";
                    for (String cn : c.getColumnNames()) {
                        str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
                    }
                    Log.d("LOG_TAG", str);
                } while (c.moveToNext());
            }
        } else
            Log.d("LOG_TAG", "Cursor is null");
    }

    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            // конструктор суперкласса
            super(context, "myDB", null, 5);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d("LOG_TAG", "--- onCreate database ---");
            // создаем таблицу с полями
            db.execSQL("create table table_action ("
                    + "id integer primary key autoincrement,"
                    + "id_category integer,"
                    + "date_start integer,"
                    + "date_stop integer"
                    + ");");

            db.execSQL("create table table_category ("
                    + "id integer primary key autoincrement,"
                    + "category text" + ");");
/*
            db.execSQL("create table table_date ("
                    + "id integer primary key autoincrement,"
                    + "date VARCHAR(255)" + ");");
*/

            Log.d("LOG_TAG", "--- onCreated database ---");

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d("LOG_TAG", "--- onUpgrade database ---");
            try {
                db.execSQL("drop table table_action;");
                db.execSQL("drop table table_category;");
                //db.execSQL("drop table table_date;");
            }
            catch (Exception ex)
            {
                Log.d("LOG_TAG", "--- onUpgrade database problem --- "+ex.toString());
            }

            try {
                db.execSQL("create table table_action ("
                        + "id integer primary key autoincrement,"
                        + "id_category integer,"
                        + "date_start integer,"
                        + "date_stop integer"
                        + ");");

                db.execSQL("create table table_category ("
                        + "id integer primary key autoincrement,"
                        + "category text" + ");");

            }
                catch (Exception ex)
                {
                    Log.d("LOG_TAG", "--- onUpgrade database problem create --- "+ex.toString());
                }
        }
    }

}
