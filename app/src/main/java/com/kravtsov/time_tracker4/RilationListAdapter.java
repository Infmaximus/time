package com.kravtsov.time_tracker4;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by User on 05.08.2015.
 */
public class RilationListAdapter extends BaseAdapter {
    public Context context;
    public Context Mycontext;
    //public MainActivity Baseclass;
    public int RealCount;

    public StructSendObj  structSendObj;

    public ArrayList<SparseArray> RealString=null;
    //MainActivity Own,
    public RilationListAdapter(Context context,ArrayList<SparseArray> Grid_s_word) {
        this.context = context;
        //Baseclass=Own;
        Mycontext=context;
        RealCount=Grid_s_word.size();
        //System.arraycopy();
        RealString=Grid_s_word;
        try {

            Collections.sort(RealString, new Comparator<SparseArray>() {
                @Override
                public int compare(SparseArray lhs, SparseArray rhs) {
                    int i1 = Integer.parseInt(lhs.get(2).toString());
                    int i2 = Integer.parseInt(rhs.get(2).toString());
                    return i1 > i2 ? -1
                            : i1 < i2 ? 1
                            : 0;
                }
            });
        }
        catch (Exception ex)
        {
            Log.d("LOG_TAG","RilationListAdapter Collections "+ex.toString());
        }
    }

    @Override
    public int getCount() {
        return RealCount;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View customView = convertView;

        if (convertView == null) {
            LayoutInflater li = LayoutInflater.from(context);
            customView = li.inflate(R.layout.layoutstatistic, null);
/*
            TextView tv = (TextView) customView
                    .findViewById(R.id.item_text_view);
            tv.setText("Item " + position);

            ImageView iv = (ImageView) customView
                    .findViewById(R.id.item_image_view);
            iv.setImageResource( R.drawable.ic_launcher );
            */

        }

        TextView tv_Rilation = (TextView) customView
                .findViewById(R.id.Text_rilation);
        TextView tv_Category = (TextView) customView
                .findViewById(R.id.Text_category_rel);
        TextView tv_Diff = (TextView) customView
                .findViewById(R.id.Text_hour_rel);

        tv_Category.setText(RealString.get(position).get(0).toString());
        tv_Rilation.setText(RealString.get(position).get(1).toString());
        tv_Diff.setText(RealString.get(position).get(2).toString()+"%");

        int RealPos=position+1;
 ;
        tv_Rilation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int myint=position+1;
                /*
                Baseclass.ChangeItem=true;
                Baseclass.ChangeItemId=position;
                try {
                    Baseclass.Gridchange();
                }
                catch (Exception ex)
                {
                    Log.d("LOG_TAG3", "TextView непонятная проблема "+ex.toString());
                }
                */
                //Toast.makeText(context, ""+myint, Toast.LENGTH_LONG).show();
            }
        });
        /*
        ImageButton button= (ImageButton)customView.findViewById(R.id.buttongrid);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int myint=position+1;
                String my="btn "+myint;
                //Toast.makeText(context, my, Toast.LENGTH_LONG).show();

                try {
                    Baseclass.SetOnGrid("",position,99);
                }
                catch (Exception ex)
                {
                    Log.d("LOG_TAG3", "button непонятная проблема "+ex.toString());
                }
            }
        });
        */
        return customView;
    }

    @Override
    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}

