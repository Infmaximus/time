package com.kravtsov.time_tracker4;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

/**
 * Created by User on 20.05.2015.
 */
public class DrawWatch extends Activity implements View.OnTouchListener, SurfaceHolder.Callback {

    public SurfaceView mSurface;
    // private DrawingThread mThread;
    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    Path path;
    SurfaceHolder MysurfaceHolder;
    Context MyContex;
    MainActivity OwnContext;
    Bitmap MuBit;
    Canvas canvas;
    static int smint;
    int mint;

    //Нормальные переменные
    DisplayMetrics metricsB;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("LOG_TAG","onCreate DrawWatch start ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);

    }

    private float dpFromPx(float px) {
        return px
                / getApplicationContext().getResources().getDisplayMetrics().density;
    }

    private float pxFromDp(float dp) {
        return dp
                * getApplicationContext().getResources().getDisplayMetrics().density;
    }

    public  DrawWatch(MainActivity context,SurfaceView mySurface)
    {
        Log.d("LOG_TAG", "drawwatch ");

        OwnContext=context;

        mSurface =mySurface;
        mSurface.setOnTouchListener(this);
        mSurface.getHolder().addCallback(this);
        //mSurface.setFormat(PixelFormat.TRANSPARENT);


        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3);
        paint.setColor(Color.BLACK);


        MuBit = BitmapFactory.decodeResource(OwnContext.getResources(), R.drawable.bigc);

/*
        // узнаем размеры экрана из класса Display
        Display display = getWindowManager().getDefaultDisplay();
        metricsB = new DisplayMetrics();
        display.getMetrics(metricsB);
*/

    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent)
    {
        Log.d("LOG_TAG", "drawwatch onTouch");
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            path = new Path();
            path.moveTo(motionEvent.getX(), motionEvent.getY());
        } else if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
            path.lineTo(motionEvent.getX(), motionEvent.getY());
        } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            path.lineTo(motionEvent.getX(), motionEvent.getY());
        }

        //oval.set(100,100,600,150);

        float Mytg1;
        float Mytg2;
        float Mysin1;
        float Mysin2;
        float Mycos1;
        float Mycos2;

        float Xlen;
        float Ylen;

        float Rad=150F;
        float X0=150;
        float Y0=150;


        int pointerIndex, pointerId;

        canvas = MysurfaceHolder.lockCanvas();
        //MysurfaceHolder.setFormat(PixelFormat.TRANSPARENT);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(MuBit,0,0,paint);              //перерисовываем каждый раз часы

        //canvas.drawBitmap(MuBit,102F,50F,paint);              //перерисовываем каждый раз часы



        if(motionEvent.getX()>=X0)
        {
            Mytg1=(motionEvent.getY()-Y0)/(motionEvent.getX()-X0);
            Xlen=(float) Math.sqrt(Rad*Rad/(1+Mytg1*Mytg1));
            Ylen=Xlen*Mytg1;

            Log.d("LOG_TAG", "Xlen"+Xlen);
            Log.d("LOG_TAG", "Xlen"+Ylen);
            if(Xlen<367)
                canvas.drawLine(X0,Y0,Xlen+X0,Ylen+Y0,paint);
            MysurfaceHolder.unlockCanvasAndPost(canvas);
        }
        else
        {
            Mytg1=(motionEvent.getY()-Y0)/(motionEvent.getX()-X0);
            Xlen=(float) Math.sqrt(Rad*Rad/(1+Mytg1*Mytg1));
            Ylen=Xlen*Mytg1;

            Log.d("LOG_TAG", "Xlen"+Xlen);
            Log.d("LOG_TAG", "Xlen"+Ylen);

            canvas.drawLine(X0,Y0,X0-Xlen,-Ylen+Y0,paint);
            MysurfaceHolder.unlockCanvasAndPost(canvas);
        }





        Log.d("LOG_TAG", "X-"+motionEvent.getX()+" Y- "+motionEvent.getY());
        if (path != null) {
            // c

        }

        return true;
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder)
    {
        Log.d("LOG_TAG", "drawwatch surfaceCreated");
        float X;
        float Y;
        MysurfaceHolder=surfaceHolder;
        canvas = MysurfaceHolder.lockCanvas();
        X=canvas.getHeight();
        Y=canvas.getWidth();
        Log.d("LOG_TAG", "Set Work X-" + X + " Y-" + Y);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(MuBit,202F,50F,paint);
        MysurfaceHolder.unlockCanvasAndPost(canvas);
        //MysurfaceHolder.setFormat(PixelFormat.TRANSPARENT);

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
        Log.d("LOG_TAG", "drawwatch surfaceChanged");
        // mThread.updateSize(width, height);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder)
    {
        //mThread.quit();
        // mThread = null;
    }
}
