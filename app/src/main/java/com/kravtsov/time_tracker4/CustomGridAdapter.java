package com.kravtsov.time_tracker4;

/**
 * Created by User on 13.07.2015.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class CustomGridAdapter extends RecyclerView.Adapter<CustomGridAdapter.ViewHolder>{
    public Context context;
    public Context Mycontext;
    //public MainActivity Baseclass;
    public int RealCount;

    public StructSendObj  structSendObj;

    public ArrayList<SparseArray> RealString=null;
    //MainActivity Own,
    public CustomGridAdapter(Context context,ArrayList<SparseArray> Grid_s_word) {
        Log.d("LOG_TAG", "CustomGridAdapter CustomGridAdapter ");
        this.context = context;
        //Baseclass=Own;
        Mycontext=context;
        RealCount=Grid_s_word.size();
        //System.arraycopy();
        RealString=Grid_s_word;

    }



    public int getCount() {
        return RealCount;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        View customView = convertView;

        if (convertView == null) {
            LayoutInflater li = LayoutInflater.from(context);
            customView = li.inflate(R.layout.layoutelement, null);
        }

        TextView tv_Start = (TextView) customView
                .findViewById(R.id.Text_time_start);
        TextView tv_Stop = (TextView) customView
                .findViewById(R.id.Text_time_stop);
        TextView tv_Category = (TextView) customView
                .findViewById(R.id.Text_category);
        TextView tv_Diff = (TextView) customView
                .findViewById(R.id.Text_hour);
        try {


            tv_Start.setText(RealString.get(position).get(0).toString());
            tv_Stop.setText(RealString.get(position).get(1).toString());
            tv_Category.setText(RealString.get(position).get(2).toString());
            tv_Diff.setText(RealString.get(position).get(3).toString());

        }
        catch (Exception ex)
        {
            Log.d("LOG_TAG", "CustomGridAdapter проблема вывода "+ex.toString());
        }

        int RealPos=position+1;
        String StrForShow;

        tv_Start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int myint=position+1;
                /*
                Baseclass.ChangeItem=true;
                Baseclass.ChangeItemId=position;
                try {
                    Baseclass.Gridchange();
                }
                catch (Exception ex)
                {
                    Log.d("LOG_TAG3", "TextView непонятная проблема "+ex.toString());
                }
                */
                //Toast.makeText(context, ""+myint, Toast.LENGTH_LONG).show();
            }
        });
        /*
        ImageButton button= (ImageButton)customView.findViewById(R.id.buttongrid);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int myint=position+1;
                String my="btn "+myint;
                //Toast.makeText(context, my, Toast.LENGTH_LONG).show();

                try {
                    Baseclass.SetOnGrid("",position,99);
                }
                catch (Exception ex)
                {
                    Log.d("LOG_TAG3", "button непонятная проблема "+ex.toString());
                }
            }
        });
        */
        return customView;
    }


    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Log.d("LOG_TAG", "CustomGridAdapter onCreateViewHolder ");
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layoutelement, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Log.d("LOG_TAG", "CustomGridAdapter onBindViewHolder  ");
        try {


            viewHolder.get_tv_Start().setText(RealString.get(i).get(0).toString());
            viewHolder.tv_Stop.setText(RealString.get(i).get(1).toString());
            viewHolder.tv_Category.setText(RealString.get(i).get(2).toString());
            viewHolder.tv_Diff.setText(RealString.get(i).get(3).toString());

            viewHolder.deleteButtonListener.setRecord(RealString.get(i));
        }
        catch (Exception ex)
        {
            Log.d("LOG_TAG", "CustomGridAdapter проблема вывода "+ex.toString());
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return RealString.size();
    }


    private void delete(SparseArray record) {
        int position = RealString.indexOf(record);
        RealString.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_Start;
        public TextView tv_Stop;
        public TextView tv_Category;
        public TextView tv_Diff;
        private ImageButton  btDel;
        private DeleteButtonListener deleteButtonListener;
        public ViewHolder(View itemView) {
            super(itemView);
            Log.d("LOG_TAG", "CustomGridAdapter ViewHolder ");
            tv_Start = (TextView) itemView
                    .findViewById(R.id.Text_time_start);
            tv_Stop = (TextView) itemView
                    .findViewById(R.id.Text_time_stop);
            tv_Category = (TextView) itemView
                    .findViewById(R.id.Text_category);
            tv_Diff = (TextView) itemView
                    .findViewById(R.id.Text_hour);
            btDel=(ImageButton)itemView.findViewById(R.id.Delete);

            deleteButtonListener = new DeleteButtonListener();
            btDel.setOnClickListener(deleteButtonListener);

            tv_Start.setText("1");
            tv_Stop.setText("2");
            tv_Category.setText("3");
            tv_Diff.setText("4");
        }

        public TextView get_tv_Start(){
            return tv_Start;
        }


    }
    /*
    private class CopyButtonListener implements View.OnClickListener {
        private SparseArray record;

        @Override
        public void onClick(View v) {
            copy(record);
        }

        public void setRecord(SparseArray record) {
            this.record = record;
        }
    }
*/
    private class DeleteButtonListener implements View.OnClickListener {
        private SparseArray record;

        @Override
        public void onClick(View v) {
            delete(record);
        }

        public void setRecord(SparseArray record) {
            this.record = record;
        }
    }

}

