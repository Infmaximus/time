package com.kravtsov.time_tracker4;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

/**
 * Created by User on 06.08.2015.
 */
public class Calculate {

    public static int rilation_Time(int hour_min)
    {
        int ansver=0;
        ansver=hour_min*100/1440;
        return ansver;
    }
                                                                                                    //проверить колстарт и колстоп
    public static int diff_Time(int start_Time,int stop_Time)
    {
        long convert_to_long;
        int ansver=0;
        Calendar cal_start= Calendar.getInstance();
        Calendar cal_stop= Calendar.getInstance();
        convert_to_long=start_Time;
        convert_to_long=convert_to_long*1000;
        cal_start.setTimeInMillis(convert_to_long);
        convert_to_long=stop_Time;
        convert_to_long=convert_to_long*1000;
        cal_stop.setTimeInMillis(convert_to_long);
        ansver=cal_stop.get(cal_stop.HOUR_OF_DAY)*60+cal_stop.get(cal_stop.MINUTE)-cal_start.get(cal_start.HOUR_OF_DAY)*60-cal_start.get(cal_start.MINUTE);

        return ansver;
    }

    public static int diff_Time(Calendar start_Time,Calendar stop_Time)
    {
        int ansver=0;
        ansver=stop_Time.get(stop_Time.HOUR_OF_DAY)*60+stop_Time.get(stop_Time.MINUTE)-start_Time.get(start_Time.HOUR_OF_DAY)*60-start_Time.get(start_Time.MINUTE);

        return ansver;
    }

    public static int diff_Time_hour(int time)
    {
        int ansver=0;
        ansver=time/60;

        return ansver;
    }

    public static int diff_Time_minute(int time)
    {
        int ansver=0;
        ansver=time%60;

        return ansver;
    }

    public static long StartStop_day(long CalDay,boolean Start_or_Stop)
    {
        long result=0;
        Calendar cal=Calendar.getInstance();
        cal.setTimeInMillis(CalDay*1000);
        if(Start_or_Stop) {

            cal.set(cal.HOUR_OF_DAY,0);
            cal.set(cal.MINUTE,0);
        }
        else {
            cal.set(cal.HOUR_OF_DAY,23);
            cal.set(cal.MINUTE,59);
        }
        result=cal.getTimeInMillis() / 1000;
        Log.d("LOG_TAG", "StartStop_day  " + result);
        return result;
    }

    // Метод возвращает список для вывода в виде перечня
    public static ArrayList<SparseArray> list_all(ArrayList<StructSendObj> list_Object,Context context)
    {
        ArrayList<SparseArray> result = new ArrayList<>();

        try {

            Collections.sort(list_Object, new Comparator<StructSendObj>() {
                @Override
                public int compare(StructSendObj lhs, StructSendObj rhs) {
                    int i1 = lhs.Start_time;
                    int i2 = rhs.Start_time;
                    return i1 > i2 ? -1
                            : i1 < i2 ? 1
                            : 0;
                }
            });
        }
        catch (Exception ex)
        {
            Log.d("LOG_TAG","CustomGridAdapter Collections"+ex.toString());
        }

        for (StructSendObj RealStructSendObj:list_Object)
        {

            SparseArray array = new SparseArray();
            String StrForShow;
            Calendar cal_for_start = Calendar.getInstance();
            Calendar cal_for_stop = Calendar.getInstance();

            long VarConverttoLong=RealStructSendObj.Start_time;
            long VarMultForLong=(VarConverttoLong*1000);
            cal_for_start.setTimeInMillis(VarMultForLong);

            VarConverttoLong=RealStructSendObj.Stop_time;
            VarMultForLong=(VarConverttoLong*1000);
            cal_for_stop.setTimeInMillis(VarMultForLong);

            Calendar calendar = Calendar.getInstance();
            //SimpleDateFormat format = new SimpleDateFormat("yyyy d MM, H:mm");
            SimpleDateFormat format = new SimpleDateFormat("H:mm");
            try
            {
                array.put(0, format.format(cal_for_start.getTime())+" -");
                Log.d("LOG_TAG", "tv_Start "+format.format(cal_for_start.getTime()));
                array.put(1, format.format(cal_for_stop.getTime()) + " ");
                Log.d("LOG_TAG", "tv_Stop "+format.format(cal_for_stop.getTime()));
                array.put(2, RealStructSendObj.Categoty);

                try {
                    String[] Category = context.getResources().getStringArray(R.array.Tipelist);
                    array.put(2, "" + Category[RealStructSendObj.Categoty] + " ");
                }
                catch (Exception ex)
                {
                    Log.d("LOG_TAG", "getResources() "+ex.toString());
                }

                Log.d("LOG_TAG", "tv_Category " + RealStructSendObj.Categoty);
                int Mydiff_h=diff_Time_hour(Calculate.diff_Time(RealStructSendObj.Start_time, RealStructSendObj.Stop_time));
                int Mydiff_m=diff_Time_minute(Calculate.diff_Time(RealStructSendObj.Start_time, RealStructSendObj.Stop_time));
                if(Mydiff_h==0)
                {
                    array.put(3,"" +Mydiff_m + " мин ");
                }
                else if(Mydiff_m==0)
                {
                    array.put(3,"" + Mydiff_h + " ч ");
                }
                else
                {
                    array.put(3, "" + Mydiff_h + " ч, " +Mydiff_m + " мин ");
                }
                Log.d("LOG_TAG", "tv_Diff "+Mydiff_h+", "+ Mydiff_m);
            }
            catch (Exception ex)
            {
                Log.d("LOG_TAG", "непонятная проблема "+ex.toString());
            }


            result.add(array);

        }
        return result;
    }

    // Метод возвращает список для вывода в виде процентов
    public static ArrayList<SparseArray> list_category(ArrayList<StructSendObj> list_Object,Context context)
    {
        ArrayList<SparseArray> result = new ArrayList<>();
        String[] Category = context.getResources().getStringArray(R.array.Tipelist);
        int[] ArrayCategory = new int[Category.length];
        for (StructSendObj RealObj:list_Object) {
                ArrayCategory[RealObj.Categoty]=ArrayCategory[RealObj.Categoty]+diff_Time(RealObj.Start_time,RealObj.Stop_time);
        }
        int i=0;
        int time_all=0;
        int relation_all=0;
        for(int Summ:ArrayCategory)
        {

            if(Summ!=0)
            {
                SparseArray array = new SparseArray();
                try {
                    String[] Category_str = context.getResources().getStringArray(R.array.Tipelist);
                    array.put(0, "" + Category_str[i] + " ");
                }
                catch (Exception ex)
                {
                    Log.d("LOG_TAG", "getResources() "+ex.toString());
                }
                int Mydiff_h=diff_Time_hour(Summ);
                int Mydiff_m=diff_Time_minute(Summ);
                if(Mydiff_h==0)
                {
                    array.put(1,"" +Mydiff_m + " мин ");
                }
                else if(Mydiff_m==0)
                {
                    array.put(1,"" + Mydiff_h + " ч ");
                }
                else
                {
                    array.put(1, "" + Mydiff_h + " ч, " +Mydiff_m + " мин ");
                }
                Log.d("LOG_TAG", "tv_Diff "+Mydiff_h+", "+ Mydiff_m);

                array.put(2,rilation_Time(Summ));
                result.add(array);

                time_all= time_all+Summ;                                                            //высчитываем остатоквремени и %
                relation_all=relation_all+rilation_Time(Summ);
            }
            i++;
        }

        SparseArray array = new SparseArray();

        time_all=1440-time_all;
        relation_all=100-relation_all;
        array.put(0, "Прочее ");
        int Mydiff_h=diff_Time_hour(time_all);
        int Mydiff_m=diff_Time_minute(time_all);
        if(Mydiff_h==0)
        {
            array.put(1,"" +Mydiff_m + " мин ");
        }
        else if(Mydiff_m==0)
        {
            array.put(1,"" + Mydiff_h + " ч ");
        }
        else
        {
            array.put(1, "" + Mydiff_h + " ч, " +Mydiff_m + " мин ");
        }
        array.put(2,relation_all);

        result.add(array);
        return result;
    }

    //public int start_time_in


    //Метод разбиения времени по дням
    public static ArrayList<SparseArray> cut_for_day(Long start_Time,Long stop_Time)
    {
        ArrayList<SparseArray> result = new ArrayList<>();

        Calendar    cal_start_Time=Calendar.getInstance();
        Calendar    cal_stop_Time=Calendar.getInstance();
        boolean     cicle_work=false;
        int         day_start;
        int         day_stop;
        int         incr;
        long        correcttime;

        cal_start_Time.setTimeInMillis(start_Time*1000);
        cal_stop_Time.setTimeInMillis(stop_Time*1000);

        try {
            do
            {
                SparseArray array_element = new SparseArray();
                if(cal_start_Time.get(cal_start_Time.DAY_OF_MONTH)>cal_stop_Time.get(cal_stop_Time.DAY_OF_MONTH))
                {
                    cicle_work=true;
                    Log.d("LOG_TAG","cut_for_day stop<start");
                }
                if(cal_start_Time.get(cal_start_Time.DAY_OF_MONTH)==cal_stop_Time.get(cal_stop_Time.DAY_OF_MONTH)){
                    cicle_work=true;
                    array_element.put(0,cal_start_Time.getTimeInMillis()/1000);
                    array_element.put(1,cal_stop_Time.getTimeInMillis()/1000);
                }
                else
                {
                    array_element.put(0,cal_start_Time.getTimeInMillis()/1000);
                    array_element.put(1,StartStop_day(cal_start_Time.getTimeInMillis()/1000,false));

                    incr=cal_start_Time.get(cal_start_Time.DAY_OF_MONTH)+1;
                    cal_start_Time.set(cal_start_Time.DAY_OF_MONTH,incr);

                    correcttime=StartStop_day(cal_start_Time.getTimeInMillis()/1000,true);
                    cal_start_Time.setTimeInMillis(correcttime*1000);
                }
                result.add(array_element);
            }
            while (!cicle_work);
        }
        catch (Exception ex)
        {
            Log.d("LOG_TAG","cut_for_day "+ex.toString());
        }

        return result;
    }
}
