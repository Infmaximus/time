package com.kravtsov.time_tracker4;

import android.app.Activity;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.math.MathContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends ActionBarActivity {


    //Элементы
    static RecyclerView My_list;
    static ListView rilation_list;
    private ActionBarDrawerToggle toggle;
    public DrawWatch myDrawWatch;
    public SurfaceView mySurfaceView;

    //Класс работы со временем
    public TimeChecker myTimeChecker;

    //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //для календаря
    //-----global var-----
    public Date DateForRemember;
    //Время начала
    public Time TimeForRemember=new Time();
    //Время окончания
    public Time TimeForIf=new Time();
    //Времена в виде календаря
    public Calendar CalForStart=Calendar.getInstance();;
    public Calendar CalForStop=Calendar.getInstance();;
    public Calendar CalForNow=Calendar.getInstance();;
    //Логический выбор
    public boolean TimeStop;
    public boolean TimeStartCheck;
    public boolean TimeStopCheck;
    public boolean Time_Animation_yellow_already_use;
    public boolean Time_Animation_green_already_use;
    //-----вкладка выбора даты------
    public static Spinner spinner;
    //public static Spinner spinnerTime;
    static final int DATE_DIALOG_ID = 0;
    static final int TIME_DIALOG_ID = 1;

    public int currentYear;
    public int currentMonth;
    public int currentDay;
    public int currentHour;
    public int currentMinute;

    public int TypeOfCommand;
    public Time TimeForRememberCommand=new Time();

    public boolean StartCall;
    public boolean StartCallConfig;
    public Time TimeForRememberStartCall=new Time();
    public boolean StopCall;
    public boolean StopCallConfig;
    public Time TimeForRememberStopCall=new Time();
    public boolean StartConf;
    public boolean StartConfConfig;
    public Time TimeForRememberStartConf=new Time();
    public boolean StopConf;
    public boolean StopConfConfig;
    public Time TimeForRememberStopConf=new Time();



    public ImageButton floating_action_but;    //Кнопка старта сейчас
    public ImageButton floating_action_but_stop;    //Кнопка стопа сейчас
    public ImageButton floating_action_ok_but;   //Кнопка старта/стопа
    public ImageButton floating_action_ok_but2;    //Кнопка старта/стопа
    public Button flat_start_time_but;   //Кнопка старта по времени
    public Button flat_stop_time_but;    //Кнопка стопа по времени
    public Button flat_date_but;    //Кнопка стопа по времени

    public Context Mycontext;

    public static final String APP_PREFERENCES = "mysettings";
    public SharedPreferences start_Time_Now_Pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //для портретного режима

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(toggle);

        ListView lv_navigation_drawer = (ListView) findViewById(R.id.lv_navigation_drawer);
        lv_navigation_drawer.setAdapter(new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                new String[] {"Screen 1", "Screen 2", "Screen 3"}));

        //mySurfaceView=(SurfaceView)findViewById(R.id.surfaceView);
        //mySurfaceView.setZOrderOnTop(true);    // necessary
        //myDrawWatch=new DrawWatch(this,mySurfaceView);
        myTimeChecker=new TimeChecker(this,this);
        //Работа с датой
        //Отложенный запуск
        spinner = (Spinner)findViewById(R.id.spinner);
        My_list=(RecyclerView)findViewById(R.id.listView2);
        rilation_list=(ListView)findViewById(R.id.listView);

        Mycontext=this;

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                String[] choose = getResources().getStringArray(R.array.Tipelist);
                TypeOfCommand=selectedItemPosition;
            }
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        floating_action_but=(ImageButton) findViewById(R.id.floating_action_but);
        floating_action_but_stop=(ImageButton) findViewById(R.id.floating_action_but_stop);
        floating_action_ok_but=(ImageButton) findViewById(R.id.floating_action_but_yellow);
        floating_action_ok_but2=(ImageButton) findViewById(R.id.floating_action_green);
        flat_start_time_but=(Button) findViewById(R.id.Button_set_start_time);
        flat_stop_time_but=(Button) findViewById(R.id.Button_set_stop_time);
        flat_date_but=(Button) findViewById(R.id.button_set_date);

        try {
            if (savedInstanceState != null) {
                Log.d("LOG_TAG","onCreate savedInstanceState вошел");
                try {
                floating_action_ok_but.setVisibility(savedInstanceState.getBoolean("floating_action_ok_but") ? View.VISIBLE : View.GONE);
                floating_action_ok_but2.setVisibility(savedInstanceState.getBoolean("floating_action_ok_but2") ? View.VISIBLE : View.GONE);
                floating_action_but.setVisibility(savedInstanceState.getBoolean("floating_action_but") ? View.VISIBLE : View.GONE);
                floating_action_but_stop.setVisibility(savedInstanceState.getBoolean("floating_action_but_stop") ? View.VISIBLE : View.GONE);
            }
            catch (Exception ex)
            {
                Log.d("LOG_TAG","onCreate savedInstanceState ошибка"+ex.toString());
            }
            } else {
                Log.d("LOG_TAG","onCreate savedInstanceState не вошел");
                floating_action_ok_but.setVisibility(View.GONE);
                floating_action_ok_but2.setVisibility(View.GONE);
                floating_action_but_stop.setVisibility(View.GONE);
            }
        }
        catch (Exception exe)
        {
            Log.d("LOG_TAG","onCreate savedInstanceState ошибка"+exe.toString());
        }

        currentYear=CalForStart.get(CalForStart.YEAR);
        currentMonth=CalForStart.get(CalForStart.MONTH)+1;
        currentDay=CalForStart.get(CalForStart.DAY_OF_MONTH);

        flat_date_but.setText(time_HM_Show(currentDay,currentMonth,".")+'.'+currentYear);




        //загружаем информацию в list
        CalForStart.set(currentYear,currentMonth,currentDay,1, 1, 1);
        ArrayList<StructSendObj> MyList1=myTimeChecker.Request2_Date(CalForStart.getTimeInMillis() / 1000);

        //RecyclerViewAdapter adapter = new RecyclerViewAdapter(records);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        My_list.setAdapter(new CustomGridAdapter(Mycontext,Calculate.list_all(MyList1,Mycontext)));
        //recyclerView.setAdapter(adapter);
        My_list.setLayoutManager(layoutManager);
        My_list.setItemAnimator(itemAnimator);


        rilation_list.setAdapter(new RilationListAdapter(Mycontext,Calculate.list_category(MyList1,Mycontext)));




                                                                                                    //долгие нажатия вызывают начало и окончание времени
        flat_start_time_but.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                TimeForRemember.set(1, 0, 0, currentDay, currentMonth, currentYear);
                CalForStart.set(currentYear, currentMonth, currentDay, 0, 0, 1);
                flat_start_time_but.setText("c "+time_HM_Show(0,0,":"));
                return true;
            }
        });
        flat_stop_time_but.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                TimeForIf.set(1, 59, 23, currentDay, currentMonth, currentYear);
                CalForStop.set(currentYear, currentMonth, currentDay, 23, 59, 1);
                flat_stop_time_but.setText("до "+time_HM_Show(23,59,":"));
                return true;
            }
        });


        start_Time_Now_Pref=getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outstate)
    {

        Log.d("LOG_TAG", "onSaveInstanceState  вход" );
        outstate.putBoolean("floating_action_ok_but", floating_action_ok_but.getVisibility() == View.VISIBLE);
        outstate.putBoolean("floating_action_ok_but2",floating_action_ok_but2.getVisibility()==View.VISIBLE);
        outstate.putBoolean("floating_action_but",floating_action_but.getVisibility()==View.VISIBLE);
        outstate.putBoolean("floating_action_but_stop",floating_action_but_stop.getVisibility()==View.VISIBLE);

        super.onSaveInstanceState(outstate);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state)
    {
        Log.d("LOG_TAG", "onRestoreInstanceState  вход");

        floating_action_ok_but.setVisibility(state.getBoolean("floating_action_ok_but") ? View.VISIBLE : View.GONE);
        floating_action_ok_but2.setVisibility(state.getBoolean("floating_action_ok_but2") ? View.VISIBLE : View.GONE);
        floating_action_but.setVisibility(state.getBoolean("floating_action_but") ? View.VISIBLE : View.GONE);
        floating_action_but_stop.setVisibility(state.getBoolean("floating_action_but_stop") ? View.VISIBLE : View.GONE);

        super.onSaveInstanceState(state);
        //outstate.putBoolean("floating_action_but",floating_action_ok_but.getVisibility()==View.VISIBLE);
    }

    //При нажатии на любую кнопку
    public void onClick(View v) {
        //ArrayList word_in_sentence=new ArrayList();
        //sqh = new CreateBD(this);
        // База нам нужна для записи и чтения
        //sqdb = sqh.getWritableDatabase();
        try
        {
            switch (v.getId())
            {
                case R.id.Button_set_start_time:
                    try
                    {
                        TimeStop=false;                                                             //Cейчас выбирается начальное время
                        showDialog(TIME_DIALOG_ID);



                    }
                    catch (Exception ex)
                    {
                        Log.d("LOG_TAG", "fail tread "+ex.toString());

                    }
                    break;
                case R.id.Button_set_stop_time:
                    try
                    {
                        TimeStop=true;                                                              //Cейчас выбирается конечное время
                        showDialog(TIME_DIALOG_ID);
                    }
                    catch (Exception ex)
                    {
                        Log.d("LOG_TAG", "fail tread "+ex.toString());
                    }
                    break;
                case R.id.button_set_date:
                    try
                    {
                        showDialog(DATE_DIALOG_ID);


                    }
                    catch (Exception ex)
                    {
                        Log.d("LOG_TAG", "fail tread "+ex.toString());
                    }
                    break;
                case R.id.floating_action_but:                                                      //кнопка начать действие сейчас
                    try
                    {

                        CalForNow=Calendar.getInstance();
                        int Monts=CalForNow.get(CalForNow.MONTH)+1;
                        CalForNow.set(CalForNow.MONTH,Monts);
                        CalForNow.set(CalForNow.SECOND,1);
                        SharedPreferences.Editor edit = start_Time_Now_Pref.edit();
                        edit.putLong("Start", CalForNow.getTimeInMillis()/1000);
                        edit.putInt("Category", TypeOfCommand);
                        edit.commit();


                        floating_action_but_stop.setVisibility(View.VISIBLE);                            //делаем зеленую кнопку видимой
                        Animation animation = AnimationUtils.loadAnimation(Mycontext, R.anim.mytrans_ok_down);
                        Animation animation2 = AnimationUtils.loadAnimation(Mycontext, R.anim.mytrans_ok_up);
                        animation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation arg0) {
                                //Functionality here
                                floating_action_but.setVisibility(View.GONE);                        //делаем желтую кнопку невидимой
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        floating_action_but.startAnimation(animation);
                        floating_action_but_stop.startAnimation(animation2);



                        Long My=start_Time_Now_Pref.getLong("Start",0);
                        CalForNow.setTimeInMillis(My*1000);

                    }
                    catch (Exception ex)
                    {
                        Log.d("LOG_TAG", "fail floating_action_but "+ex.toString());
                    }
                    break;
                case R.id.floating_action_but_stop:                                                 //кнопка окончить действие сейчас
                    try
                    {
                        Long StartTime=start_Time_Now_Pref.getLong("Start",0);
                        CalForNow=Calendar.getInstance();
                        int Monts=CalForNow.get(CalForNow.MONTH)+1;
                        CalForNow.set(CalForNow.MONTH,Monts);
                        CalForNow.set(CalForNow.SECOND,1);
                        int k;



                        if(StartTime!=CalForNow.getTimeInMillis()/1000)
                            k=myTimeChecker.IncertWithMulti(StartTime,CalForNow.getTimeInMillis()/1000,start_Time_Now_Pref.getInt("Category",0));
                        else
                            show_toast(getString(R.string.toast_equal));
                        show_lists();
                                                                                                    //блок анимации
                                                                                                    //продумать, как автоматизировать
                        floating_action_but.setVisibility(View.VISIBLE);                            //делаем зеленую кнопку видимой
                        Animation animation = AnimationUtils.loadAnimation(Mycontext, R.anim.mytrans_ok_down);
                        Animation animation2 = AnimationUtils.loadAnimation(Mycontext, R.anim.mytrans_ok_up);
                        animation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation arg0) {
                                //Functionality here
                                floating_action_but_stop.setVisibility(View.GONE);                        //делаем желтую кнопку невидимой
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        floating_action_but_stop.startAnimation(animation);
                        floating_action_but.startAnimation(animation2);
                    }
                    catch (Exception ex)
                    {
                        Log.d("LOG_TAG", "fail floating_action_but_stop "+ex.toString());
                    }
                    break;
                case R.id.floating_action_green:    //нажатие не зеленую кнопку
                    try
                    {

                        //добавляем действие
                        //myTimeChecker.IncertWithSort(CalForStart.getTimeInMillis()/1000,CalForStop.getTimeInMillis()/1000,TypeOfCommand);
                        int k;
                        if(CalForStart.getTimeInMillis()/1000!=CalForStop.getTimeInMillis()/1000)
                            k=myTimeChecker.IncertWithSort(CalForStart.getTimeInMillis()/1000,CalForStop.getTimeInMillis()/1000,TypeOfCommand);
                        else
                            show_toast(getString(R.string.toast_equal));
                        show_lists();


                        Time_Animation_green_already_use=false;                                     //зеленая кнопка снова может появится
                        Time_Animation_yellow_already_use=false;                                    //Говорим, что желтая кнопка снова может появится
                        TimeStartCheck=false;
                        TimeStopCheck=false;
                        flat_start_time_but.setText(R.string.start_time);
                        flat_stop_time_but.setText(R.string.stop_time);
                        //myTimeChecker.Request2_Date();
                        Animation animation2 = AnimationUtils.loadAnimation(Mycontext, R.anim.mytrans_ok_down);
                        animation2.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }
                            @Override
                            public void onAnimationEnd(Animation arg0) {
                                //Functionality here
                                floating_action_ok_but2.setVisibility(View.GONE);                   //делаем желтую кнопку невидимой

                                /*                                                                    //загружаем информацию в list
                                ArrayList<StructSendObj> MyList1=myTimeChecker.Request2_Date(CalForStart.getTimeInMillis() / 1000);
                                My_list.setAdapter(new CustomGridAdapter(Mycontext,Calculate.list_all(MyList1,Mycontext)));
                                rilation_list.setAdapter(new RilationListAdapter(Mycontext,Calculate.list_category(MyList1,Mycontext)));
                                */
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        floating_action_ok_but2.startAnimation(animation2);
                    }
                    catch (Exception ex)
                    {
                        Log.d("LOG_TAG", "fail tread "+ex.toString());
                    }
                    break;
                case R.id.floating_action_but_yellow:    //нажатие не желтую кнопку
                    try
                    {
                        String strShow="";
                        if(TimeStop) {
                            TimeStop = false;                                                       //Cейчас выбирается начальное время
                            strShow=this.getString(R.string.toast_start);
                        }
                        else {
                            TimeStop = true;                                                        //Cейчас выбирается конечное время
                            strShow=this.getString(R.string.toast_stop);
                        }

                        showDialog(TIME_DIALOG_ID);
                        Toast toast = Toast.makeText(getApplicationContext(),
                                strShow, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                    catch (Exception ex)
                    {
                        Log.d("LOG_TAG", "fail tread "+ex.toString());
                    }
                    break;
                default:
                    break;
            }
        }
        catch (Exception ex)
        {
            //Log.d("LOG_TAG", "onClick fail"+ex.toString());
        }
        //sqdb.close();
       // sqh.close();
    }


    private void show_lists()                                                                       //отображение lists
    {
        ArrayList<StructSendObj> MyList1=myTimeChecker.Request2_Date(CalForStart.getTimeInMillis() / 1000);
        My_list.setAdapter(new CustomGridAdapter(Mycontext,Calculate.list_all(MyList1,Mycontext)));
        rilation_list.setAdapter(new RilationListAdapter(Mycontext,Calculate.list_category(MyList1,Mycontext)));
    }

    private void show_toast(String text)                                                                       //отображение toast
    {
        Toast toast = Toast.makeText(getApplicationContext(),
                text, Toast.LENGTH_SHORT);
        toast.show();
    }

    public String time_HM_Show(int Hour,int Minute,String separate)
    {
        String result="";
        if(Hour<10)
            result="0"+Hour;
        else
            result=""+Hour;

        if(Minute<10)
            result=result+separate+"0"+Minute;
        else
            result=result+separate+Minute;
                return result;
    }

    public void ShowBtn()
    {
        floating_action_ok_but.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.mytrans_ok);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationEnd(Animation arg0) {
                //Functionality here

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        floating_action_ok_but.startAnimation(animation);
    }
    //Работа со временем
    private DatePickerDialog.OnDateSetListener reservationDate = new DatePickerDialog.OnDateSetListener()
    {
        public void onDateSet(DatePicker view, int year, int month, int day)
        {
            if (true)
            {
                Log.d("LOG_TAG", "Try  NO"+
                        "Your rental time is set for "
                        + (month + 1) + "-" + day + "-" + year + " to "
                        + (month + 1) + "-" + (day + 3) + "-" + year + " in "
                        + ".");
            }
            if (true)
            {
                Log.d("LOG_TAG", "Try  NO"+
                        "Your rental time is set for "
                        + (month + 1) + "-" + day + "-" + year + " to "
                        + (month + 1) + "-" + (day + 3) + "-" + year + " in "
                        + ".");
            }
            currentYear=year;
            currentMonth=month+1;
            currentDay=day;
            flat_date_but.setText(time_HM_Show(currentDay,currentMonth,".")+'.'+currentYear);

            try {
                CalForStart.set(currentYear,currentMonth,currentDay,1, 1, 1);
                CalForStop.set(currentYear,currentMonth,currentDay,1, 1, 1);

                show_lists();                                                                       //отображаем list
            }
            catch (Exception ex)
            {
                Log.d("LOG_TAG","Error "+ex.toString());
            }

            try
            {

            }
            catch (Exception ex)
            {

            }
            //CalForStart.add(Calendar.YEAR,currentYear);
            //CalForStart.set(Calendar.YEAR,currentYear);
            //CalForStart.set(currentYear,currentMonth,currentDay,1, 1, 1);
            //CalForStop.set(currentYear,currentMonth,currentDay,1, 1, 1);

            TimeForRemember.set(1, currentMinute, currentHour, currentDay, currentMonth, currentYear);
            TimeForIf.set(1, currentMinute, currentHour, currentDay, currentMonth, currentYear);
			            /*
			            DateForRemember.setYear(year);
			            DateForRemember.setMonth(month);
			            DateForRemember.setDate(day);
			            */

        }
    };

    public TimePickerDialog.OnTimeSetListener timeDate = new TimePickerDialog.OnTimeSetListener()
    {
        public void onTimeSet(TimePicker view, int hours, int minutes) {
            boolean what_choice=false;
            Log.d("LOG_TAG", "TimePickerDialog Your arrival time will be at " + hours + ":"
                    + minutes + ".");
            currentHour = hours;
            currentMinute = minutes;
            if (!TimeStop) {
                TimeForRemember.set(1, currentMinute, currentHour, currentDay, currentMonth, currentYear);

                CalForStart.set(currentYear, currentMonth, currentDay, currentHour, currentMinute, 1);

                TimeStartCheck = true;
                //Более красивый выыод
                flat_start_time_but.setText("c "+time_HM_Show(currentHour,currentMinute,":"));
            } else {
                TimeForIf.set(1, currentMinute, currentHour, currentDay, currentMonth, currentYear);
                CalForStop.set(currentYear, currentMonth, currentDay, currentHour, currentMinute, 1);
                TimeStopCheck = true;
                flat_stop_time_but.setText(" до "+time_HM_Show(currentHour,currentMinute,":"));
            }
            try {

                if (TimeStartCheck && TimeStopCheck)
                {
                    if(Calculate.diff_Time(CalForStart,CalForStop)<0)
                    {
                        if(TimeStop)
                        {
                            TimeForIf.set(1, 59, 23, currentDay, currentMonth, currentYear);
                            CalForStart.set(currentYear, currentMonth, currentDay, 0, 0, 1);
                            flat_start_time_but.setText("c "+time_HM_Show(0,0,":"));

                            Toast toast = Toast.makeText(getApplicationContext(),
                                    getString(R.string.toast_stop_small), Toast.LENGTH_SHORT);
                            toast.show();
                        }
                        else
                        {
                            TimeForRemember.set(1, 0, 0, currentDay, currentMonth, currentYear);
                            CalForStop.set(currentYear, currentMonth, currentDay, 23, 59, 1);
                            flat_stop_time_but.setText("до "+time_HM_Show(23,59,":"));
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    getString(R.string.toast_start_big), Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    }
                }

                if (TimeStartCheck && TimeStopCheck && (!Time_Animation_green_already_use)) {
                                                                                                    //Спрашиваем, есть ли уже зеленая кнопка?
                    //TimeForRemember.
                    floating_action_ok_but2.setVisibility(View.VISIBLE);                            //делаем зеленую кнопку видимой
                    Animation animation = AnimationUtils.loadAnimation(Mycontext, R.anim.mytrans_ok_down);
                    Animation animation2 = AnimationUtils.loadAnimation(Mycontext, R.anim.mytrans_ok_up);
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation arg0) {
                            //Functionality here
                            floating_action_ok_but.setVisibility(View.GONE);                        //делаем желтую кнопку невидимой
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    Time_Animation_green_already_use=true;                                          //Говорим, что зеленая кнопка не может появится
                    floating_action_ok_but.startAnimation(animation);
                    floating_action_ok_but2.startAnimation(animation2);

                } else if(!Time_Animation_yellow_already_use) {                                     //Спрашиваем, есть ли уже желтая кнопка?
                    Time_Animation_yellow_already_use=true;                                         //Говорим, что желтая кнопка больше не может появится
                    if (TimeStartCheck) {
                        floating_action_ok_but.setVisibility(View.VISIBLE);
                        Animation animation = AnimationUtils.loadAnimation(Mycontext, R.anim.mytrans_ok_up);
                        floating_action_ok_but.startAnimation(animation);
                    } else {
                        floating_action_ok_but.setVisibility(View.VISIBLE);
                        Animation animation = AnimationUtils.loadAnimation(Mycontext, R.anim.mytrans_ok_up);
                        floating_action_ok_but.startAnimation(animation);
                    }
                }
            } catch (Exception ex) {
                Log.d("LOG_TAG","Error "+ex.toString());
            }
        }
    };

    protected Dialog onCreateDialog(int id)
    {
        switch(id)
        {
            case DATE_DIALOG_ID:
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);
                return new DatePickerDialog(this, reservationDate, year, month, day);
            case TIME_DIALOG_ID:
            {
                TimeForRemember.setToNow();
                int hour =Time.HOUR;
                int minute = Time.MINUTE;
                //int
                return new TimePickerDialog(this, timeDate, TimeForRemember.hour, TimeForRemember.minute, true);
            }
        }
        return null;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }
}